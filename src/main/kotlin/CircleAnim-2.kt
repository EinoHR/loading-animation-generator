import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolatedWithTarget
import org.openrndr.draw.loadFont
import org.openrndr.draw.renderTarget
import org.openrndr.extra.gui.GUI
import org.openrndr.extra.parameters.*
import org.openrndr.ffmpeg.VideoWriter
import org.openrndr.shape.Circle
import org.openrndr.shape.Rectangle
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.PI
import kotlin.math.sin

enum class FPSOption {
    fps16,
    fps25,
    fps30,
    fps50,
    fps60,
    fps120,
    fps240,
}

fun main() = application {
    configure {
        width = 800
        height = 800
    }

    program {
        var videoWriter = VideoWriter.create().size(width, height)
        val videoTarget = renderTarget(width, height) {
            colorBuffer()
        }

        var recordingStopped = false
        var recording = false
        var recordingStartTime = 0.0

        val gui = GUI()
        val settings = object {
            @IntParameter("spinSpeed", 0, 10)
            var spinSpeed: Int = 1

            @IntParameter("sizeSpeed", 0, 10)
            var sizeSpeed: Int = 1

            @DoubleParameter("spinOffset", 0.0, Math.PI*2)
            var spinOffset: Double = 1.0

            @DoubleParameter("sizeOffset", 0.0, Math.PI)
            var sizeOffset: Double = 1.0

            @DoubleParameter("circleSize", 10.0, 300.0)
            var circleSize: Double = 50.0

            @DoubleParameter("dotSize", 5.0, 40.0)
            var dotSize: Double = 10.0

            @IntParameter("circleAmount", 1, 500)
            var circleAmount: Int = 5

            @BooleanParameter("drawHelpCircles", 0)
            var drawHelpCircles: Boolean = false

            @BooleanParameter("drawTrails", 0)
            var drawTrails: Boolean = false


            @OptionParameter("renderFramerate", order = 11)
            var renderFramerate = FPSOption.fps60

            @ActionParameter("startRecording", order = 10)
            fun startRecording() {
                fun renderFramerate() : Int {
                    return when (this.renderFramerate) {
                        FPSOption.fps16 -> 16
                        FPSOption.fps25 -> 25
                        FPSOption.fps30 -> 30
                        FPSOption.fps50 -> 50
                        FPSOption.fps60 -> 60
                        FPSOption.fps120 -> 120
                        FPSOption.fps240 -> 240
                    }
                }

                fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
                    val formatter = SimpleDateFormat(format, locale)
                    return formatter.format(this)
                }
                val date = Calendar.getInstance().time
                val dateInString = date.toString("yyyy-M-dd_HH:mm:ss")
                videoWriter.output("output_${dateInString}.mp4").frameRate(renderFramerate()).start()
                recordingStartTime = seconds
                recording = true
            }
        }


        gui.add(settings, "Settings")

//                Formula (copied from WolframAlpha) to calculate the period of the size animation function
        var sizePeriod = -(2*PI)/settings.sizeSpeed
//                Formula (copied from WolframAlpha) to calculate the period of the size animation function
        var spinPeriod = -(2*PI)/settings.spinSpeed
//                The common period of two functions is the LCM of their respective periods
        var commonPeriod = LCM(sizePeriod, spinPeriod)

        val font = loadFont("data/fonts/default.otf", 48.0)

        extend(gui)
        extend {
            val circle = Circle((width/2).toDouble(), (height/2).toDouble(), settings.circleSize)

            val drawCircles : MutableList<Circle> = mutableListOf()
            val helpCircles : MutableList<Circle> = mutableListOf()

            for (i in 0 until settings.circleAmount) {
                val helpCircle = Circle(circle.center, circle.radius*sin(settings.sizeSpeed*seconds+i*settings.sizeOffset))
                val drawCircle = Circle(getAnglePointOnCircle(Math.PI*(sin(settings.spinSpeed*seconds+i*settings.spinOffset)), helpCircle), settings.dotSize)
                drawCircles.add(drawCircle)
                helpCircles.add(helpCircle)
            }

            drawer.isolatedWithTarget(videoTarget) {
                if (settings.drawTrails) {
                    drawer.fill = ColorRGBa(0.0,0.0,0.0,0.2)
                    drawer.rectangle(Rectangle(0.0,0.0, width.toDouble(), height.toDouble()))
                } else {
                    drawer.clear(ColorRGBa.BLACK)
                }
                drawer.fill = ColorRGBa.WHITE
                drawer.stroke = null
                drawer.circles(drawCircles)
            }
            drawer.image(videoTarget.colorBuffer(0))

            if (recording) {
                drawer.fontMap = font
                drawer.fill = ColorRGBa.WHITE
                drawer.text("Recording...", 220.0, 50.0)
            }

            if (settings.drawHelpCircles) {
                drawer.fill = null
                drawer.stroke = ColorRGBa.WHITE
                drawer.strokeWeight = 3.0
                drawer.circles(helpCircles)
            }

            if (recording) {
                videoWriter.frame(videoTarget.colorBuffer(0))

//                Formula (copied from WolframAlpha) to calculate the period of the size animation function
                sizePeriod = -(2*PI)/settings.sizeSpeed
//                Formula (copied from WolframAlpha) to calculate the period of the size animation function
                spinPeriod = -(2*PI)/settings.spinSpeed
//                The common period of two functions is the LCM of their respective periods
                commonPeriod = LCM(sizePeriod, spinPeriod)

                if (recordingStartTime-seconds < commonPeriod) {
                    recordingStopped = true
                    println("Recording finished")
                }
            }
            if (recording && recordingStopped) {
                videoWriter.stop()
                videoWriter = VideoWriter.create().size(width, height)
                recording = false
                recordingStopped = false
            }
        }
    }
}