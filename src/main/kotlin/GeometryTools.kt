import org.openrndr.extra.noise.random
import org.openrndr.math.Vector2
import org.openrndr.shape.Circle
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

fun getRandomPointOnCircle(circle: Circle) : Vector2 {
    return getAnglePointOnCircle(random(0.0, 360.0), circle)
}

fun getAnglePointOnCircle(angle : Double, circle: Circle) : Vector2 {
    var x = circle.radius * sin(angle)
    var y = circle.radius * cos(angle)

    return Vector2(circle.center.x+x, circle.center.y+y)
}

fun getDistanceBetweenPoints(point1: Vector2, point2: Vector2): Double {
    var dx = point2.x - point1.x
    var dy = point2.y - point1.y

    return sqrt(dx * dx + dy * dy)
}

fun getPointOnTwoPointLine(line : MutableList<Vector2>, fac : Double) : Vector2 {
    var x = ((1-fac) * line.first().x + fac * line.last().x)
    var y = ((1-fac) * line.first().y + fac * line.last().y)
    return Vector2(x, y)
}

// Least Common Multiple
fun LCM(a: Double, b: Double): Double {
    val biggerNum = if(a > b) a else b
    var lcm = biggerNum
    while(true) {
        // Break the loop when we get number divisible by a and b.
        if(((lcm % a) == 0.0) && ((lcm % b) == 0.0)) {
            break
        }
        lcm  += biggerNum
    }
    return lcm
}