# Loading animation generator

This is a small OPENRNDR program that can generate loading animation -style loops using circles.

The code is kotlin and lives in src/main/kotlin/.

The shell script `build.sh` is all that is needed to build, output is a `distribution` folder (and `distribution.zip` file for portability) which contains everything needed to run the program.
The releases tab contains a `distribution.zip` built on my system.

The program has been tested using openjdk 18.0.1.1 on Arch Linux.

## Examples

![](examples/duos.gif)
![](examples/space.gif)
![](examples/electric-field.gif)
![](examples/spinny.gif)

# Licensing

![](free-software-logo_type_inverted.svg)

The code for this program is licensed under [GPLv3](COPYING.md), a free software license.

All animations created by the program are licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/).  
Appropriate attribution does not have to be right next to the generated animation, it can be hidden on a credits page.  
If you post to social media any animations created by the program, you need to include a link to this page with the post.